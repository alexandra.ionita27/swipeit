package com.example.swipeit.entity;

import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import java.util.Date;


public abstract class AbstractEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "created_at", defaultValue = "CURRENT_TIMESTAMP")
    private Date createdAt;
    @ColumnInfo(name = "updated_at", defaultValue = "CURRENT_TIMESTAMP")
    private Date updateAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }
}
