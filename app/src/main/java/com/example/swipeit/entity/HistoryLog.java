package com.example.swipeit.entity;

import android.location.Location;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.example.swipeit.utils.HistoryStatusEnum;

@Entity
public class HistoryLog extends AbstractEntity {

    @ColumnInfo(name = "status")
    private HistoryStatusEnum status;
    @ColumnInfo(name = "value_obtained")
    private double valueObtained;
    @ColumnInfo(name = "location")
    private String location;

    public HistoryStatusEnum getStatus() {
        return  this.status;
    }

    public void setStatus(HistoryStatusEnum status) {
        this.status = status;
    }

    public double getValueObtained() {
        return valueObtained;
    }

    public void setValueObtained(double valueObtained) {
        this.valueObtained = valueObtained;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

