package com.example.swipeit.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "userId",
        onDelete = CASCADE))
public class Profile extends AbstractEntity {
    @ColumnInfo(name = "avg_duration")
    private double avgDuration;
    @ColumnInfo(name = "avg_acceleration")
    private double[] avgAcceleration;
    @ColumnInfo(name = "avg_touch_duration")
    private double avgTouchDuration;
    @ColumnInfo(name = "avg_writing_duration")
    private double avgWritingDuration;
    private int userId;

    public double getAvgDuration() {
        return avgDuration;
    }

    public void setAvgDuration(double avgDuration) {
        this.avgDuration = avgDuration;
    }

    public double[] getAvgAcceleration() {
        return avgAcceleration;
    }

    public void setAvgAcceleration(double[] avgAcceleration) {
        this.avgAcceleration = avgAcceleration;
    }

    public double getAvgTouchDuration() {
        return avgTouchDuration;
    }

    public void setAvgTouchDuration(double avgTouchDuration) {
        this.avgTouchDuration = avgTouchDuration;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getAvgWritingDuration() {
        return avgWritingDuration;
    }

    public void setAvgWritingDuration(double avgWritingDuration) {
        this.avgWritingDuration = avgWritingDuration;
    }
}
