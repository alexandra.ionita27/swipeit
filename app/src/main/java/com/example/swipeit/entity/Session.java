package com.example.swipeit.entity;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

import static androidx.room.ForeignKey.SET_NULL;

@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "userId",
        onDelete = SET_NULL))
public class Session extends AbstractEntity {
    @ColumnInfo(name = "acceleration")
    private double[] acceleration;
    @ColumnInfo(name = "duration")
    private double duration;
    @ColumnInfo(name = "touch_duration")
    private Double touchDuration;
    @ColumnInfo(name = "writing_duration")
    private Double writingDuration;
    @Nullable
    private Integer userId;

    public double getAccelerationAverage(){
        double sum=0;
        for(double accelerationValue:acceleration){
            sum+=accelerationValue;
        }
        return sum / acceleration.length;
    }

    public double[] getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double[] acceleration) {
        this.acceleration = acceleration;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public Double getTouchDuration() {
        return touchDuration;
    }

    public void setTouchDuration(Double touchDuration) {
        this.touchDuration = touchDuration;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
            this.userId = userId;
    }

    public Double getWritingDuration() {
        return writingDuration;
    }

    public void setWritingDuration(Double writingDuration) {
        this.writingDuration = writingDuration;
    }
}
