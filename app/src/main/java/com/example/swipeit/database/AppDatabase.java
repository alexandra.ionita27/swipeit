package com.example.swipeit.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.swipeit.converter.Converters;
import com.example.swipeit.dao.HistoryDao;
import com.example.swipeit.dao.ProfileDao;
import com.example.swipeit.dao.SessionDao;
import com.example.swipeit.dao.UserDao;
import com.example.swipeit.entity.HistoryLog;
import com.example.swipeit.entity.Profile;
import com.example.swipeit.entity.Session;
import com.example.swipeit.entity.User;

@Database(entities = {User.class, Profile.class, Session.class, HistoryLog.class}, version = AppDatabase.DATABASE_VERSION)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    public static AppDatabase appDatabase;
    public static AppDatabase getInstance(Context context){
        if(appDatabase==null) {
           appDatabase = Room.databaseBuilder(context,
                    AppDatabase.class, "swipeit")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return appDatabase;
    }

    public static final int DATABASE_VERSION = 15;

    public abstract UserDao userDao();

    public abstract ProfileDao profileDao();

    public abstract SessionDao sessionDao();

    public abstract HistoryDao historyDao();
}
