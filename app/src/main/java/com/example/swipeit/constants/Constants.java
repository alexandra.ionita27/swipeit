package com.example.swipeit.constants;

public class Constants {
    public static final int NOTIFICATION_ID = 1;
    public static final String NOTIFICATION_CHANNEL_ID = "CHANNEL_1";
    public static final String NOTIFICATION_CHANNEL_NAME = "CHANNEL_1";

    public static final String ABSTRACT_SERVICE_TAG="ABSTRACT_SERVICE";
    public static final String BACKGROUND_SERVICE_TAG="BACKGROUND_SERVICE";
    public static final String LOCATION_SERVICE_TAG="LOCATION_SERVICE";
    public static final String PROFILE_MANAGEMENT_TAG="PROFILE_MANAGEMENT";
    public static final String HISTORY_MANAGEMENT_TAG="HISTORY_LOG_MANAGEMENT";
    public static final String MAIN_ACTIVITY_TAG="MAIN_ACTIVITY";


    public static final String USER_ID="userId";
    public static final String HAS_PROFILE="hasProfile";

    public static final int HISTORY_LOG_NUMBER=3;

}
