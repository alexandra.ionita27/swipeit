package com.example.swipeit.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.swipeit.entity.Profile;

import java.util.List;

@Dao
public interface ProfileDao {
    @Query("SELECT * FROM profile")
    List<Profile> getAll();

    @Query("SELECT * FROM profile WHERE id IN (:profileIds)")
    List<Profile> loadAllByIds(int[] profileIds);

    @Query("Select * from profile LIMIT 1")
    Profile loadOne();

    @Insert
    void insertAll(Profile... profiles);

    @Delete
    void delete(Profile profile);


}
