package com.example.swipeit.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.swipeit.entity.HistoryLog;

import java.util.List;

@Dao
public interface HistoryDao {
    @Query("SELECT * FROM historylog")
    List<HistoryLog> getAll();

    @Query("SELECT * FROM historylog WHERE id IN (:historyLogs)")
    List<HistoryLog> loadAllByIds(int[] historyLogs);

    @Query("SELECT * FROM historylog order by created_at DESC limit :limit")
    List<HistoryLog> loadLatestFive(int limit);

    @Insert
    void insertAll(HistoryLog... historyLogs);

    @Delete
    void delete(HistoryLog historyLog);
}
