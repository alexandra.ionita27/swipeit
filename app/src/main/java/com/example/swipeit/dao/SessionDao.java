package com.example.swipeit.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.util.TableInfo;

import com.example.swipeit.entity.Profile;
import com.example.swipeit.entity.Session;

import java.util.List;
import java.util.Optional;

@Dao
public interface SessionDao {
    @Query("SELECT * FROM session")
    List<Session> getAll();

    @Query("SELECT * FROM session WHERE id IN (:sessionIds)")
    List<Session> loadAllByIds(int[] sessionIds);

    @Query("Select * from session where userId= :userId")
    List<Session> loadAllForUser(int userId);
    @Query("Select * from session where userId is null order by created_at desc limit 1")
    Optional<Session> loadLatestSessionWithoutUser();
    @Query("Select acceleration from session where userId= :userId")
    List<double[]> loadAccelerationAllForUser(int userId);
    @Query("Select touch_duration from session where userId= :userId")
    double[] loadTouchDurationAllForUser(int userId);
    @Query("Select duration from session where userId= :userId")
    double[] loadDurationAllForUser(int userId);
    @Query("Select writing_duration from session where userId= :userId")
    double[] loadWritingDurationAllForUser(int userId);
    @Query("DELETE FROM session WHERE userId is null")
    void deleteAllWithoutUser();

    @Insert
    void insertAll(Session... sessions);

    @Delete
    void delete(Session session);
}
