package com.example.swipeit.utils;

public enum HistoryStatusEnum {
    SUCCESS("Success"),
    FAILURE("Failure");

    private String stringValue;

    HistoryStatusEnum(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }
}
