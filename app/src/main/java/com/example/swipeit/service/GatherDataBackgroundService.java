package com.example.swipeit.service;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.swipeit.MainActivity;
import com.example.swipeit.R;
import com.example.swipeit.bll.HistoryLogManagementBll;
import com.example.swipeit.bll.PermissionCheckBll;
import com.example.swipeit.bll.ProfileCreateBll;
import com.example.swipeit.bll.ProfileManagementBll;
import com.example.swipeit.bll.SessionManagementBll;
import com.example.swipeit.database.AppDatabase;
import com.example.swipeit.entity.Profile;
import com.example.swipeit.utils.HistoryStatusEnum;

import java.util.Timer;
import java.util.TimerTask;

import static com.example.swipeit.constants.Constants.BACKGROUND_SERVICE_TAG;
import static com.example.swipeit.constants.Constants.HAS_PROFILE;
import static com.example.swipeit.constants.Constants.NOTIFICATION_CHANNEL_ID;
import static com.example.swipeit.constants.Constants.NOTIFICATION_ID;
import static com.example.swipeit.constants.Constants.USER_ID;

public class GatherDataBackgroundService extends AbstractDataService {
    ProfileManagementBll profileManagementBll;
    ProfileCreateBll profileCreateBll;
    SessionManagementBll sessionManagementBll;
    HistoryLogManagementBll historyLogManagementBll;
    Profile profile;
    DevicePolicyManager deviceManger;
    boolean hasProfile;
    int sessionCount = 0;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        AppDatabase db = AppDatabase.getInstance(getApplicationContext());
        sessionManagementBll = new SessionManagementBll(db.sessionDao());
        profileCreateBll = new ProfileCreateBll(db.profileDao(), sessionManagementBll);
        profileManagementBll = new ProfileManagementBll(db.profileDao(), sessionManagementBll);
        historyLogManagementBll = new HistoryLogManagementBll(db.historyDao());
        deviceManger = (DevicePolicyManager)
                getSystemService(Context.DEVICE_POLICY_SERVICE);
        this.userId = intent.getIntExtra(USER_ID, 0);
        this.userId = this.userId == 0 ? null : userId;
        this.hasProfile = intent.getBooleanExtra(HAS_PROFILE, false);
        startForeground();
        final Timer timer = new Timer();
        if (hasProfile) {
            Log.i(BACKGROUND_SERVICE_TAG, "The user has profile");
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    profile = profileManagementBll.getProfileForUser();

                }
            });
            t.start();
            synchronized (t) {
                try {
                    t.wait(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Intent intent1 = new Intent(this, LocationService.class);
            startService(intent1);
            TimerTask timer_task = new TimerTask() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void run() {
                    try {
                        Log.i(BACKGROUND_SERVICE_TAG, "Creating session when  profile");
                        createSession(null);
                        double distance = profileManagementBll.getValueForDistance(profile);
                        if (distance == -1) {
                            return;
                        }
                        if (distance < 2) {
                            Log.i(BACKGROUND_SERVICE_TAG, "Insert success");
                            historyLogManagementBll.insertHistoryLog(historyLogManagementBll.createHistoryLog(HistoryStatusEnum.SUCCESS, distance, location));
                        } else {
                            Log.i(BACKGROUND_SERVICE_TAG, "Insert failure");
                            historyLogManagementBll.insertHistoryLog(historyLogManagementBll.createHistoryLog(HistoryStatusEnum.FAILURE, distance, location));
                        }
                        if (!historyLogManagementBll.isUserTheOwner()) {
                            Log.i(BACKGROUND_SERVICE_TAG, "User is an impostor");
                            lockPhone();
                        } else {
                            Log.i(BACKGROUND_SERVICE_TAG, "User is the owner");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            timer.scheduleAtFixedRate(timer_task, 20*1000, 20 * 1000);
        } else {
            Log.i(BACKGROUND_SERVICE_TAG, "The user dose NOT have a profile");

            final TimerTask timer_task = new TimerTask() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void run() {
                    try {
                        Log.i(BACKGROUND_SERVICE_TAG, "Creating session when no profile");
                        Log.i(BACKGROUND_SERVICE_TAG, "Session count: " + sessionCount);
                        createSession(userId);
                        sessionCount++;
                        if (sessionCount > 7) {
                            Log.i(BACKGROUND_SERVICE_TAG, "We should stop");
                            profileCreateBll.persistProfileForUser(userId);
                            Log.i(BACKGROUND_SERVICE_TAG, "Profile created");
                            timer.cancel();
                            timer.purge();
                            stopForeground(true);
                            System.exit(0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            };
            timer.scheduleAtFixedRate(timer_task, 20*1000, 20 * 1000);

        }
        return START_NOT_STICKY;
    }


    private void startForeground() {
        Intent notificationIntent = new Intent(this, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        startForeground(NOTIFICATION_ID, new NotificationCompat.Builder(this,
                NOTIFICATION_CHANNEL_ID)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Service is running background")
                .setContentIntent(pendingIntent)
                .build());

    }



    public void lockPhone() {
        deviceManger.lockNow();
    }

}


