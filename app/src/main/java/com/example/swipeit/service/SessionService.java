package com.example.swipeit.service;

import com.example.swipeit.database.AppDatabase;
import com.example.swipeit.entity.Session;

import java.util.Calendar;

public class SessionService {
    private AppDatabase appDatabase;

    public SessionService(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    public synchronized Session  createSession(double[] accelerationAverage, double durationAverage, double writingDuration, double touchSizeAverage, Integer userId ){
        Session session=new Session();
        session.setAcceleration(accelerationAverage);
       session.setDuration(durationAverage);
       session.setWritingDuration(writingDuration);
       session.setTouchDuration(touchSizeAverage);
       session.setUserId(userId);
       session.setCreatedAt(Calendar.getInstance().getTime());
       session.setUpdateAt(Calendar.getInstance().getTime());
       this.appDatabase.sessionDao().insertAll(session);

       return session;
    }
}
