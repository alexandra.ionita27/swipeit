package com.example.swipeit.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.gesture.GestureOverlayView;
import android.graphics.PixelFormat;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.swipeit.database.AppDatabase;

import java.util.Arrays;

import static com.example.swipeit.constants.Constants.ABSTRACT_SERVICE_TAG;

@RequiresApi(api = Build.VERSION_CODES.O)
public abstract class AbstractDataService extends Service implements View.OnTouchListener, SensorEventListener {
    private SensorManager sensorManager;
    private float writingFrequencySum = 0;
    private float touchFrequencySum = 0;
    private double[] accelerationSum;
    private int accelerationCounter = 0;
    private static final int ACCELERATION_NUMBER = 5;
    private float durationSum = 0;
    private int touchDownCounter = 0;
    private int touchCounter = 0;
    private int writingCounter = 0;
    public float startTime;
    private SessionService sessionService;
    protected Integer userId;
    private double lastTouch;
    public static Location location;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        final AppDatabase db = AppDatabase.getInstance(getApplicationContext());
        sessionService = new SessionService(db);
        this.accelerationSum = new double[3];
        sensorManager = (SensorManager) getApplication().getSystemService(Context.SENSOR_SERVICE);
        sensorManager.registerListener(AbstractDataService.this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 1);
        final GestureOverlayView touchLayout = new GestureOverlayView(this);
        touchLayout.setOnTouchListener(this);


        final WindowManager mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        final int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        mWindowManager.addView(touchLayout, mParams);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (accelerationCounter < ACCELERATION_NUMBER) {
                accelerationSum[0] += event.values[0];
                accelerationSum[1] += event.values[1];
                accelerationSum[2] += event.values[2];
                accelerationCounter++;
            }
        }

    }

    public double[] getAccelerationAverage() {
        for (int i = 0; i < this.accelerationSum.length; i++) {
            this.accelerationSum[i] /= ACCELERATION_NUMBER;
        }
        return this.accelerationSum;
    }


    public float getWritingDurationAverage() {
        return this.writingCounter == 0 ? 0 : this.writingFrequencySum / this.writingCounter;
    }

    public float getTouchDurationAverage() {
        return this.touchCounter == 0 ? 0 : this.touchFrequencySum / this.touchCounter;
    }

    public float getDurationAverage() {

        return this.touchDownCounter == 0 ? 0 : this.durationSum / this.touchDownCounter;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        double durationBtwTwoTouches = 0;
        Log.i(ABSTRACT_SERVICE_TAG, "Last event time: " + lastTouch);
        Log.i(ABSTRACT_SERVICE_TAG, "Event Downtime: " + event.getEventTime());
        startTime = event.getDownTime();
        touchDownCounter++;
        if (lastTouch != 0) {
            durationBtwTwoTouches = (event.getEventTime() - lastTouch) / 1000;
        }
        Log.i(ABSTRACT_SERVICE_TAG, "Duration between two events:" + durationBtwTwoTouches);
        if (durationBtwTwoTouches < 1.5 && durationBtwTwoTouches > 0) {
            Log.i(ABSTRACT_SERVICE_TAG, "Is writing");
            writingFrequencySum += durationBtwTwoTouches;
            writingCounter++;
        }
        if (durationBtwTwoTouches >= 1.5) {
            Log.i(ABSTRACT_SERVICE_TAG, "Is NOT writing");
            touchFrequencySum += durationBtwTwoTouches;
            touchCounter++;
        }

        durationSum += ((double) (android.os.SystemClock.uptimeMillis() - event.getDownTime()) / 1000);
        Log.i(ABSTRACT_SERVICE_TAG, "Duration:" + (android.os.SystemClock.uptimeMillis() - event.getDownTime()));
        Log.i(ABSTRACT_SERVICE_TAG, "Writing Duration" + writingFrequencySum);
        Log.i(ABSTRACT_SERVICE_TAG, "Touch duration" + touchFrequencySum);
        lastTouch = android.os.SystemClock.uptimeMillis();
        return false;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void createSession(final Integer userIdParam) throws InterruptedException {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(ABSTRACT_SERVICE_TAG, "Session duration: " + getDurationAverage());
                if (getDurationAverage() == 0 || (getWritingDurationAverage() == 0 && getTouchDurationAverage() == 0)) {
                    Log.i(ABSTRACT_SERVICE_TAG, "Not inserting session as duration is 0");

                } else {
                    sessionService.createSession(getAccelerationAverage(), getDurationAverage(), getWritingDurationAverage(), getTouchDurationAverage(), userIdParam);

                    Log.i(ABSTRACT_SERVICE_TAG, "Inserting session...");
                }
                resetSessionParams();
            }
        });
        t.start();
        synchronized (t) {
            t.wait(300);
        }


    }

    public void resetSessionParams() {
        writingFrequencySum = 0;
        touchFrequencySum = 0;
        accelerationCounter = 0;
        durationSum = 0;
        touchDownCounter = 0;
        touchCounter=0;
        writingCounter=0;
        Arrays.fill(accelerationSum, 0);
    }



}
