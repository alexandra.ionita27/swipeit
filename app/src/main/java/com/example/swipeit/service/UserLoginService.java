package com.example.swipeit.service;


import com.example.swipeit.database.AppDatabase;
import com.example.swipeit.entity.User;

public class UserLoginService {
    private AppDatabase appDatabase;


    public UserLoginService(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    public User createUser(String userName) {
        User newUser = new User();
        newUser.setNickname(userName);
        this.appDatabase.userDao().insertAll(newUser);
        return newUser;
    }

    public User getUserByNickname(String nickname) {
        return this.appDatabase.userDao().loadAllByNickname(nickname);
    }

    public User getUserById(Integer id) {
        return this.appDatabase.userDao().loadAllByIds(id).get(0);
    }

    public int getUserCount() {
        return this.appDatabase.userDao().getAll().size();
    }
}
