package com.example.swipeit;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.swipeit.bll.PermissionCheckBll;
import com.example.swipeit.bll.ProfileCreateBll;
import com.example.swipeit.bll.ProfileManagementBll;
import com.example.swipeit.bll.SessionManagementBll;
import com.example.swipeit.database.AppDatabase;
import com.example.swipeit.entity.Profile;
import com.example.swipeit.entity.User;
import com.example.swipeit.service.DeviceAdmin;
import com.example.swipeit.service.GatherDataBackgroundService;
import com.example.swipeit.service.SessionService;
import com.example.swipeit.service.UserLoginService;

import static com.example.swipeit.constants.Constants.HAS_PROFILE;
import static com.example.swipeit.constants.Constants.MAIN_ACTIVITY_TAG;
import static com.example.swipeit.constants.Constants.NOTIFICATION_CHANNEL_ID;
import static com.example.swipeit.constants.Constants.NOTIFICATION_CHANNEL_NAME;
import static com.example.swipeit.constants.Constants.USER_ID;

public class MainActivity extends AppCompatActivity {
    Button btnSubmit;
    UserLoginService userLoginService;
    SessionService sessionService;
    User user;
    ProfileCreateBll profileCreateBll;
    SessionManagementBll sessionManagementBll;
    ProfileManagementBll profileManagementBll;
    DevicePolicyManager deviceManger;
    ComponentName compName;
    boolean hasProfile;
    EditText edtUsername;
    String nickName;

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        deviceManger = (DevicePolicyManager)
                getSystemService(Context.DEVICE_POLICY_SERVICE);
        compName = new ComponentName(this, DeviceAdmin.class);
        final AppDatabase db = AppDatabase.getInstance(getApplicationContext());
        btnSubmit = findViewById(R.id.btn_submit);
        edtUsername = findViewById(R.id.edtUsername);
        userLoginService = new UserLoginService(db);
        sessionService = new SessionService(db);
        sessionManagementBll = new SessionManagementBll(db.sessionDao());
        profileCreateBll = new ProfileCreateBll(db.profileDao(), sessionManagementBll);
        profileManagementBll = new ProfileManagementBll(db.profileDao(), sessionManagementBll);

        PermissionCheckBll.checkAndGrantPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        PermissionCheckBll.checkAndGrantPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
        PermissionCheckBll.checkAndGrantPermission(MainActivity.this, Manifest.permission.ACCESS_BACKGROUND_LOCATION);
        PermissionCheckBll.checkAndGrantPermission(MainActivity.this, Manifest.permission.FOREGROUND_SERVICE);
        PermissionCheckBll.checkAndGrantPermission(MainActivity.this, Manifest.permission.SYSTEM_ALERT_WINDOW);

        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                hasProfile = profileManagementBll.hasProfile();
            }
        });
        t3.start();
        try {
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (hasProfile) {
            Thread t4 = new Thread(new Runnable() {
                @Override
                public void run() {
                    Profile profile = profileManagementBll.getProfileForUser();
                    user = userLoginService.getUserById(profile.getUserId());
                }
            });
            t4.start();
            try {
                t4.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            enableOverlay();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nickName = edtUsername.getText().toString().trim();
                if (isNicknameValid()) {
                    enableOverlay();
                }
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void continueT() {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                if (user == null) {
                    userLoginService.createUser(nickName);
                    user = userLoginService.getUserByNickname(nickName);
                }

            }
        });
        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        if (hasProfile) {
            createNotificationChannel();
            Intent intent = new Intent(this, GatherDataBackgroundService.class);
            intent.putExtra(USER_ID, user.getId());
            intent.putExtra(HAS_PROFILE, true);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.setFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
            startForegroundService(intent);
            this.finish();
            return;
        }

        createNotificationChannel();
        Intent intent = new Intent(this, GatherDataBackgroundService.class);
        intent.putExtra(USER_ID, user.getId());
        intent.putExtra(HAS_PROFILE, false);
        startForegroundService(intent);
        this.finish();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, importance);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void enablePhone() {
        boolean active = deviceManger.isAdminActive(compName);
        if (active) {
            continueT();
        } else {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, compName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "You should enable the app!");
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

            startActivityForResult(intent, 10);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void enableOverlay() {
        if (!Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getApplicationContext().getPackageName()));
            intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, 20);
        } else {
            enablePhone();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10) {
            Log.i(MAIN_ACTIVITY_TAG, "Finished with phone enabeling");
            continueT();
        }
        if (requestCode == 20) {
            Log.i(MAIN_ACTIVITY_TAG, "Overlay enabled");
            enablePhone();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int i = 0; i < permissions.length; i++) {
            Log.i(MAIN_ACTIVITY_TAG, permissions[i] + " " + grantResults[i]);
        }

    }

    public boolean isNicknameValid() {
        if (nickName.isEmpty()) {
            edtUsername.setError("Invalid Nickname");
            return false;
        }
        return true;
    }
}