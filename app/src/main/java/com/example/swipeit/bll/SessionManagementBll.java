package com.example.swipeit.bll;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.swipeit.dao.SessionDao;
import com.example.swipeit.entity.Session;

import java.util.List;
import java.util.Optional;

public class SessionManagementBll {
    SessionDao sessionDao;

    public SessionManagementBll(SessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }

    public double[][] getAccelerationArrayForUser(int userId){
        List<double[]> list=sessionDao.loadAccelerationAllForUser(userId);
        double[][] result=new double[list.size()][3];
        for(int i=0;i<list.size();i++){
            double[] array=list.get(i);
            System.arraycopy(array, 0, result[i], 0, array.length);
        }
       return result;
    }
    public double[] getDurationArrayForUser(int userId){
        return sessionDao.loadDurationAllForUser(userId);
    }
    public double[] getWritingDuration(int userId){
        return sessionDao.loadWritingDurationAllForUser(userId);
    }
    public double[] getTouchDuration(int userId){
        return sessionDao.loadTouchDurationAllForUser(userId);
    }

    public void deleteAllWithoutUserId(){
        sessionDao.deleteAllWithoutUser();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Session getLatestSessionWithoutUser(){
        Optional<Session> session=sessionDao.loadLatestSessionWithoutUser();
        if(session.isPresent()){
            return session.get();
        }
        return null;
    }
}
