package com.example.swipeit.bll;

import com.example.swipeit.dao.ProfileDao;
import com.example.swipeit.entity.Profile;

import java.util.Calendar;

public class ProfileCreateBll {
    ProfileDao profileDao;
    SessionManagementBll sessionManagementBll;

    public ProfileCreateBll(ProfileDao profileDao, SessionManagementBll sessionManagementBll) {
        this.profileDao = profileDao;
        this.sessionManagementBll = sessionManagementBll;
    }

    public double[] getAccelerationMedian(int userId) {
        return MathBll.getMedianForMatrix(sessionManagementBll.getAccelerationArrayForUser(userId),3,3);
    }

    public double getDurationStandardDeviation(int userId) {
        return MathBll.getMedian(sessionManagementBll.getDurationArrayForUser(userId));
    }

    public double getTouchSizeStandardDeviation(int userId) {
        return MathBll.getMedian(sessionManagementBll.getWritingDuration(userId));
    }

    public double getPressureStandardDeviation(int userId) {
        return MathBll.getMedian(sessionManagementBll.getTouchDuration(userId));
    }

    public Profile createProfile(int userId) {
        Profile profile = new Profile();
        profile.setAvgAcceleration(getAccelerationMedian(userId));
        profile.setAvgDuration(getDurationStandardDeviation(userId));
        profile.setAvgTouchDuration(getTouchSizeStandardDeviation(userId));
        profile.setAvgWritingDuration(getPressureStandardDeviation(userId));
        profile.setCreatedAt(Calendar.getInstance().getTime());
        profile.setUpdateAt(Calendar.getInstance().getTime());
        profile.setUserId(userId);

        return profile;
    }

    public synchronized void persistProfileForUser(int userId) {
        profileDao.insertAll(createProfile(userId));

    }
}
