package com.example.swipeit.bll;

import java.util.List;

public class MathBll {
    public static double getMedian(double[] values){
        float sum=0;
        for (double value : values) {
            sum += value;
        }
        return sum/values.length;
    }
    public static double[] getMedianForMatrix(double[][] values, int noColumns, int noLines){
        double[] sum=new double[noColumns];
        for(int i=0;i<noColumns;i++){
            for(int j=0;j<noLines;j++)
                sum[i]+=values[j][i];
        }
        for(int i=0;i<noColumns;i++){
            sum[i]/=noColumns;
        }
        return sum;
    }

    public static double getEuclideanDistanceForArray(double[] array, double[] arrayCompare){
        double sum=0;
        for(int i=0;i<array.length;i++){
            sum+=Math.pow(array[i]-arrayCompare[i],2);
        }
        return Math.sqrt(sum);
    }
}
