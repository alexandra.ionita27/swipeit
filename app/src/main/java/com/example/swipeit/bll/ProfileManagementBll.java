package com.example.swipeit.bll;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.swipeit.constants.Constants;
import com.example.swipeit.dao.ProfileDao;
import com.example.swipeit.entity.Profile;
import com.example.swipeit.entity.Session;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class ProfileManagementBll {
    ProfileDao profileDao;
    SessionManagementBll sessionManagementBll;

    public ProfileManagementBll(ProfileDao profileDao, SessionManagementBll sessionManagementBll) {
        this.profileDao = profileDao;
        this.sessionManagementBll=sessionManagementBll;
    }

    public Profile getProfileForUser() {
        return profileDao.loadOne();
    }

    public boolean hasProfile() {
        Profile profile = profileDao.loadOne();
        return profile != null;
    }

    public double calculateEuclideanDistance(Profile profile, Session session) {
        double sum;
        double acceleration, writingDuration, touchDuration, duration,touchWriting;
        touchDuration = Math.pow(profile.getAvgWritingDuration() - session.getWritingDuration(), 2);
        writingDuration = Math.pow(profile.getAvgWritingDuration() - session.getWritingDuration(), 2);
        acceleration =MathBll.getEuclideanDistanceForArray(profile.getAvgAcceleration(),session.getAcceleration());

        duration = Math.pow(profile.getAvgDuration() - session.getDuration(), 2);
        sum = writingDuration + acceleration  + duration-touchDuration;
        return Math.sqrt(sum);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public double getValueForDistance(Profile profile) throws Exception {
        Session session=this.sessionManagementBll.getLatestSessionWithoutUser();
        if (session == null)
            throw new Exception("Session not found exception");
        long diffInMillis = Math.abs(Calendar.getInstance().getTimeInMillis() - session.getCreatedAt().getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillis, TimeUnit.MINUTES);
        if(diff>2){
            return -1;
        }
        double distance = this.calculateEuclideanDistance(profile, session);
        Log.i(Constants.PROFILE_MANAGEMENT_TAG,"The euclidean Distance is: " + distance);
        return distance;
    }

}
