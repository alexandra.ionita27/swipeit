package com.example.swipeit.bll;

import android.icu.util.DateInterval;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.swipeit.constants.Constants;
import com.example.swipeit.dao.HistoryDao;
import com.example.swipeit.entity.HistoryLog;
import com.example.swipeit.utils.HistoryStatusEnum;

import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import static com.example.swipeit.constants.Constants.HISTORY_LOG_NUMBER;

public class HistoryLogManagementBll {
    HistoryDao historyDao;

    public HistoryLogManagementBll(HistoryDao historyDao) {
        this.historyDao = historyDao;
    }

    public HistoryLog createHistoryLog(HistoryStatusEnum status, double value, Location location) {
        HistoryLog historyLog = new HistoryLog();
        historyLog.setStatus(status);
        historyLog.setValueObtained(value);
        historyLog.setLocation(location.getLatitude()+", "+location.getLongitude());
        historyLog.setCreatedAt(Calendar.getInstance().getTime());
        return historyLog;
    }

    public void insertHistoryLog(HistoryLog historyLog) {
        this.historyDao.insertAll(historyLog);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<HistoryLog> getLatestFiveHistoryLogs(){
       List<HistoryLog> hist= historyDao.loadLatestFive(HISTORY_LOG_NUMBER);
       Log.i(Constants.HISTORY_MANAGEMENT_TAG, "History size"+hist.size());
        HistoryLog newestHistoryLog=hist.stream().max(new Comparator<HistoryLog>() {
            @Override
            public int compare(HistoryLog o1, HistoryLog o2) {
                return (int) (o1.getCreatedAt().getTime()-o2.getCreatedAt().getTime());
            }
        }).get();
        HistoryLog oldestHistoryLog=hist.stream().min(new Comparator<HistoryLog>() {
            @Override
            public int compare(HistoryLog o1, HistoryLog o2) {
                return (int) (o1.getCreatedAt().getTime()-o2.getCreatedAt().getTime());
            }
        }).get();
        Log.i("HistoryManagementBll", "Oldest: "+oldestHistoryLog.getId());
        Log.i("HistoryManagementBll", "Newest: "+newestHistoryLog.getId());
        Log.i("HistoryManagementBll", "Difference: "+ (newestHistoryLog.getCreatedAt().getTime()-oldestHistoryLog.getCreatedAt().getTime())/(1000*20));
        if((newestHistoryLog.getCreatedAt().getTime()-oldestHistoryLog.getCreatedAt().getTime())/(1000*20)>HISTORY_LOG_NUMBER-1 || newestHistoryLog.getId()==oldestHistoryLog.getId()){
            return null;
        }
       return hist;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean isUserTheOwner(){
        List<HistoryLog> historyLogs=this.getLatestFiveHistoryLogs();
        if(historyLogs==null || historyLogs.size()<HISTORY_LOG_NUMBER)
            return true;
        int count=0;
        for (HistoryLog historyLog:historyLogs) {
            if(historyLog.getStatus().equals(HistoryStatusEnum.FAILURE)){
                count+=1;
            }
        }
        Log.i(Constants.HISTORY_MANAGEMENT_TAG,"Number of failed sessions: " + count);
        return count<HISTORY_LOG_NUMBER/2+1;
    }
}
