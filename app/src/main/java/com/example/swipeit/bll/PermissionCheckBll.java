package com.example.swipeit.bll;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ServiceCompat;
import androidx.core.content.ContextCompat;

import com.example.swipeit.MainActivity;

public class PermissionCheckBll {
    private  static int REQUEST_CODE = 100;

    public static boolean hasPermission(Context context, String permission) {
        return !(ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_DENIED);
    }

    public static void grantPermission(Activity context, String permission, int requestCode) {
        ActivityCompat.requestPermissions(context, new String[]{permission}, requestCode);
    }

    public static void checkAndGrantPermission(Activity activity, String permission) {
        if (hasPermission(activity, permission)) {
            return;
        }
        grantPermission(activity, permission, REQUEST_CODE++);
    }
}
