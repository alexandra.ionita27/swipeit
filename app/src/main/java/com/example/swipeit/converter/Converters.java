package com.example.swipeit.converter;

import androidx.room.TypeConverter;

import com.example.swipeit.entity.HistoryLog;
import com.example.swipeit.utils.HistoryStatusEnum;

import org.json.JSONException;

import java.util.Date;

public class Converters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static String historyStatusEnumToString(HistoryStatusEnum historyStatusEnum) {
        return historyStatusEnum == null ? null : historyStatusEnum.getStringValue();
    }

    @TypeConverter
    public static HistoryStatusEnum stringToHistoryStatusEnum(String historyStatusStrng) {
        if(historyStatusStrng==null){
            return null;
        }
        if(historyStatusStrng.equals(HistoryStatusEnum.SUCCESS.getStringValue())){
            return HistoryStatusEnum.SUCCESS;
        }
        return HistoryStatusEnum.FAILURE;
    }

    @TypeConverter
    public static String floatArrayToString(double[] array) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < array.length - 1; i++) {
            builder.append(array[i]);
            builder.append(",");
        }
        builder.append(array[array.length - 1]);
        return builder.toString();
    }

    @TypeConverter
    public static double[] stringToFloatArray(String string){
        String[] accelerations = string.split(",");
        double[] results = new double[accelerations.length];
        for (int i = 0; i < accelerations.length; i++) {
            results[i] = Double.parseDouble(accelerations[i]);
        }

        return results;
    }

}


